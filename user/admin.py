# -*- coding: utf-8 -*-
from django.contrib import admin

from user.models import Player


class PlayerAdmin(admin.ModelAdmin):
    fields = ('username', 'email', 'lvl', 'race', 'profession', 'str', 'agi', 'wit', 'wis', 'int')
    list_display = ('username', 'email', 'lvl', 'race', 'profession')

admin.site.register(Player, PlayerAdmin)
