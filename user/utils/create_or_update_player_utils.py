# -*- coding: utf-8 -*-
from profession.models import Profession
from race.models import Race
from user.models import Player
from user.serializers.player_serializer import FullDetailedPlayerSerializer


def create_or_update_player(player_json):
    # Check for profession
    if player_json.get('prof_id'):
        # find profession with specified id
        try:
            profession = Profession.objects.filter(id=player_json.get('prof_id'))
        except Profession.DoesNotExist:
            profession = ''
    else:
        # no profession specified
        profession = ''

    # Check for race
    if player_json.get('race_id'):
        # find race with specified id
        try:
            race = Race.objects.filter(id=player_json.get('race_id'))
        except Race.DoesNotExist:
            race = ''
    else:
        # no race specified
        race = ''

    if player_json.get('id'):
        # Update existed player
        try:
            player = Player.objects.filter(id=player_json.get('id'))
        except Player.DoesNotExist:
            return {
                'error': 'Player with specified id does not exist'
            }
    else:
        # Create new player
        player = Player()
        player.username = player_json.get('username')

    player.str = player_json.get('str')
    player.agi = player_json.get('agi')
    player.wit = player_json.get('wit')
    player.wis = player_json.get('wis')
    player.int = player_json.get('int')

    player.lvl = player_json.get('lvl')

    player.exp = player_json.get('exp')

    if race and not player.race:
        player.race = race

    if profession and not player.profession:
        player.profession = profession

    player.save()

    serializer = FullDetailedPlayerSerializer(player)
    return serializer.data
