# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.db import models

from profession.models import Profession
from race.models import Race


class Player(AbstractUser):
    str = models.DecimalField(decimal_places=2, max_digits=10, default=1.00)
    agi = models.DecimalField(decimal_places=2, max_digits=10, default=1.00)
    wit = models.DecimalField(decimal_places=2, max_digits=10, default=1.00)
    wis = models.DecimalField(decimal_places=2, max_digits=10, default=1.00)
    int = models.DecimalField(decimal_places=2, max_digits=10, default=1.00)

    experience = models.IntegerField(default=0)
    lvl = models.IntegerField(default=1)

    race = models.ForeignKey(Race, blank=True, null=True)

    profession = models.ForeignKey(Profession, blank=True, null=True)
