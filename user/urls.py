# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework import routers

from user.views.crud import RegisterPlayer, PlayerCRUD

router = routers.SimpleRouter()

router.register(r'register', RegisterPlayer)
router.register(r'crud', PlayerCRUD)

urlpatterns = [
    url(r'^', include(router.urls)),
    ]
