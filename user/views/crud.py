# -*- coding: utf-8 -*-
import json

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status

from core.utils.check_user_from_request import check_user_from_request
from user.models import Player
from user.serializers.player_serializer import SimplePlayerSerializer, FullDetailedPlayerSerializer
from user.utils.create_or_update_player_utils import create_or_update_player


class PlayerCRUD(IsAuthenticated, viewsets.ModelViewSet):
    """
    C not necessary for authenticated player
    RUD method for player
    """
    queryset = Player.objects.filter(is_active=True, is_staff=False, is_superuser=False)
    serializer_class = SimplePlayerSerializer

    def create(self, request, *args, **kwargs):
        return Response(
            data=dict(message='Wrong URL to register'),
            content_type='application/json',
            status=status.HTTP_400_BAD_REQUEST
        )

    def update(self, request, *args, **kwargs):
        is_correct_user = check_user_from_request(request)
        if is_correct_user:
            data = create_or_update_player(request.data.get('player'))
            return Response(
                data=data,
                content_type='application/json',
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                data=dict(message='Logged user and user from auth token does not match'),
                content_type='application/json',
                status=status.HTTP_406_NOT_ACCEPTABLE
            )

    def list(self, request, *args, **kwargs):
        players = Player.objects.filter(
            is_active=True
        )

        serializer = SimplePlayerSerializer(players, many=True)
        return Response(
            data=dict(players_list=serializer.data),
            content_type='application/json',
            status=status.HTTP_200_OK
        )

    def retrieve(self, request, *args, **kwargs):
        is_auth_user_correct = check_user_from_request(request)
        try:
            player = Player.objects.get(id=kwargs.get('pk'))
        except Player.DoesNotExist:
            return Response(
                data=dict(message='Player with specified id does not exist'),
                content_type='application/json',
                status=status.HTTP_404_NOT_FOUND
            )

        if is_auth_user_correct:
            if request.user.username == player.username:
                serializer = FullDetailedPlayerSerializer(player)
            else:
                serializer = SimplePlayerSerializer(player)
        else:
            serializer = SimplePlayerSerializer(player)
        return Response(
            data=serializer.data,
            content_type='application/json',
            status=status.HTTP_200_OK
        )


class RegisterPlayer(AllowAny, viewsets.ModelViewSet):
    """
    Register new user (only create method)
    """
    queryset = Player.objects.filter(is_active=True, is_staff=False, is_superuser=False)
    serializer_class = SimplePlayerSerializer

    def create(self, request, *args, **kwargs):
        new_player = Player(
            username=request.data.get('username'),
            email=request.data.get('email')
        )
        new_player.save()
        new_player.set_password(request.get('password'))

        serializer = SimplePlayerSerializer(new_player)
        return Response(
            data=serializer.data,
            content_type='application/json',
            status=status.HTTP_201_CREATED
        )

    def update(self, request, *args, **kwargs):
        return Response(
            data=dict(message='Wrong URL to register'),
            content_type='application/json',
            status=status.HTTP_400_BAD_REQUEST
        )

    def list(self, request, *args, **kwargs):
        return Response(
            data=dict(message='Wrong URL to register'),
            content_type='application/json',
            status=status.HTTP_400_BAD_REQUEST
        )

    def retrieve(self, request, *args, **kwargs):
        return Response(
            data=dict(message='Wrong URL to register'),
            content_type='application/json',
            status=status.HTTP_400_BAD_REQUEST
        )
