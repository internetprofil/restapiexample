# -*- coding: utf-8 -*-
from rest_framework.serializers import ModelSerializer

from profession.serializers.profession_serializer import SimpleProfessionSerializer
from race.serializers.race_serialzier import SimpleRaceSerializer
from user.models import Player


class SimplePlayerSerializer(ModelSerializer):
    race = SimpleRaceSerializer()
    profession = SimpleProfessionSerializer()

    class Meta:
        model = Player
        fields = ('username', 'lvl', 'race', 'profession')
        read_only_fields = ('username',)


class DetailedPlayerSerializer(ModelSerializer):
    race = SimpleRaceSerializer()
    profession = SimpleProfessionSerializer()

    class Meta:
        model = Player
        fields = (
            'username',
            'lvl',
            'race',
            'profession',
            'str',
            'agi',
            'wit',
            'wis',
            'int'
        )
        read_only_fields = ('username',)


class FullDetailedPlayerSerializer(ModelSerializer):
    race = SimpleRaceSerializer()
    profession = SimpleProfessionSerializer()

    class Meta:
        model = Player
        fields = (
            'username',
            'lvl',
            'race',
            'profession',
            'str',
            'agi',
            'wit',
            'wis',
            'int',
            'experience'
        )
        read_only_fields = ('username',)
