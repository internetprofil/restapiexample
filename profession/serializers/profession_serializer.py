# -*- coding: utf-8 -*-
from rest_framework.serializers import ModelSerializer

from profession.models import Profession


class SimpleProfessionSerializer(ModelSerializer):
    class Meta:
        model = Profession
        fields = ('name',)
