# -*- coding: utf-8 -*-
from django.contrib import admin

from profession.models import Profession


class ProfessionAdmin(admin.ModelAdmin):
    fields = ('name', 'str', 'agi', 'wit', 'wis', 'int')
    list_display = ('name',)


admin.site.register(Profession, ProfessionAdmin)
