# -*- coding: utf-8 -*-
from django.db import models

PROFESSION_TYPES = (
    ('H', 'Hunter'),
    ('W', 'Warrior'),
    ('M', 'Mage'),
    ('A', 'Artisan')
)


class Profession(models.Model):
    """
    This is Profession model with stats modifiers per lvl it grants
    """
    str = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    agi = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    wit = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    wis = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    int = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    name = models.CharField(max_length=1, choices=PROFESSION_TYPES)

    # TODO bonus system with enable/disable possibility based on profession
