# -*- coding: utf-8 -*-
from django.contrib import admin

from race.models import Race


class RaceAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'str',
        'agi',
        'wit',
        'wis',
        'int'
    )
    list_display = ('name',)

admin.site.register(Race, RaceAdmin)
