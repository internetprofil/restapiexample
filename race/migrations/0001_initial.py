# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-05 15:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Race',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('str', models.DecimalField(decimal_places=2, default=1.0, max_digits=3)),
                ('agi', models.DecimalField(decimal_places=2, default=1.0, max_digits=3)),
                ('wit', models.DecimalField(decimal_places=2, default=1.0, max_digits=3)),
                ('wis', models.DecimalField(decimal_places=2, default=1.0, max_digits=3)),
                ('int', models.DecimalField(decimal_places=2, default=1.0, max_digits=3)),
                ('name', models.CharField(choices=[('H', 'Human'), ('E', 'Elf'), ('D', 'Dwarf')], max_length=1)),
            ],
        ),
    ]
