# -*- coding: utf-8 -*-
from rest_framework.serializers import ModelSerializer

from race.models import Race


class SimpleRaceSerializer(ModelSerializer):
    class Meta:
        model = Race
        fields = ('name',)
