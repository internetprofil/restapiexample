# -*- coding: utf-8 -*-
from django.db import models


RACE_TYPES = (
    ('H', 'Human'),
    ('E', 'Elf'),
    ('D', 'Dwarf')
)


class Race(models.Model):
    """
    This is Race model with stats modifiers per lvl it grants
    """
    str = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    agi = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    wit = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    wis = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)
    int = models.DecimalField(decimal_places=2, max_digits=3, default=1.00)

    name = models.CharField(max_length=1, choices=RACE_TYPES)

    # TODO maybe later some other bonus (f.e. +10% to fight with axe for dwarf)
