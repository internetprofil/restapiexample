# -*- coding: utf-8 -*-
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer


def check_user_from_request(request):
    attrs = {
        'token': request.auth.decode('utf-8')
    }
    try:
        verification_result = VerifyJSONWebTokenSerializer().validate(attrs=attrs)
    except Exception as e:
        return False
    return request.user.username == verification_result.get('user').username
