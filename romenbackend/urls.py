# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

urlpatterns = [
    # Admin site
    url(r'^manage/', admin.site.urls),
    # Get token
    url(r'^auth/', obtain_jwt_token, name='auth'),
    # Refresh token
    url(r'^auth-refresh/', refresh_jwt_token, name='refresh'),
    # Verify token
    url(r'^auth-verify/', verify_jwt_token, name='verify'),
    # Import apps urls
    url(r'player/', include("user.urls", namespace='users')),
    url(r'profession/', include("profession.urls", namespace='profession')),
    url(r'race/', include("race.urls", namespace='race')),
]
